﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.Atm
{
    public sealed class ConsolePasswordHelper
    {
        public string pwd { get; }
        public ConsolePasswordHelper(string displayText = "Введите пароль:", string mask = "*")
        {
            Console.WriteLine(displayText);
            ConsoleKey key;
            do
            {
                var keyInfo = Console.ReadKey(intercept: true);
                key = keyInfo.Key;

                if (key == ConsoleKey.Backspace && pwd.Length > 0)
                {
                    Console.Write("\b \b");
                    pwd = pwd[0..^1];
                }
                else if (!char.IsControl(keyInfo.KeyChar))
                {
                    Console.Write(mask);
                    pwd += keyInfo.KeyChar;
                }
            } while (key != ConsoleKey.Enter);
            Console.WriteLine();
        }
    }
}
