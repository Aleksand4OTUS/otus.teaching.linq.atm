﻿using System;
using System.Linq;
using Otus.Teaching.Linq.Atm;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static ATMManager _atmManager;
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            _atmManager = CreateATMManager();
            MainLoop();

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
    
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }

        static void MainLoop()
        {

            ATMService atmService = new ATMService(_atmManager);
            User user = atmService.TryAuthorize();

            if (user != null)
            {
                while (true)
                {
                    atmService.PrintMenu();
                    MenuItem menuItemNumber = atmService.GetMenuItem();

                    if (menuItemNumber == MenuItem.ExitCode)
                        break;
                    else
                        atmService.RunMenuItem(menuItemNumber);
                }
            }
            else
            {
                System.Console.ForegroundColor = ConsoleColor.Red;
                System.Console.WriteLine("Не удалось авторизоваться в системе, продолжение невозможно!");
                System.Console.ResetColor();
            }
        }

    }
}