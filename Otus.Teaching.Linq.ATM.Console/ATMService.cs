﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;

namespace Otus.Teaching.Linq.Atm
{
    public enum MenuItem
    {
        ExitCode = 0, 
        UserInfo,
        UserAccountsInfo,
        UserAccountsHistory,
        AllHistory, 
        UsersMoreThan, 
        NotExists = 100
    }
    
    public sealed class ATMService
    {
        private static ATMManager _atmManager;
        private static User _user;

        public ATMService(ATMManager atmManager) => _atmManager = atmManager;

        public User TryAuthorize()
        {
            int attempsCount = 0;
            User user = null;
            const int maxAttempsCount = 3;

            while (user == null & attempsCount < maxAttempsCount)
            {
                attempsCount++;

                Console.WriteLine("Введите логин:");
                string login = Console.ReadLine();

                ConsolePasswordHelper pwdHelper = new ConsolePasswordHelper();
                string pwd = pwdHelper.pwd;

                user = _atmManager.GetUser(login: login, password: pwd);

                if (user == null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Не найдена пара логин:пароль. Осталось попыток: {maxAttempsCount - attempsCount}");
                    Console.ResetColor();
                }
                else
                {
                    _user = user;
                }
            }

            return user;
        }


        public void PrintMenu()
        {
            Console.WriteLine($"\n{DateTime.Now} Выберите пункт меню:");
            Console.WriteLine("0. Выход из программы;");
            Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю;");
            Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя;");
            Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;");
            Console.WriteLine("4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;");
            Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой).");
        }

        public MenuItem GetMenuItem()
        {
            if (int.TryParse(System.Console.ReadLine(), out int menuItemNumber) & Enum.IsDefined(typeof(MenuItem), menuItemNumber))
            {
                return (MenuItem)menuItemNumber;
            }
            else
            {
                return MenuItem.NotExists;
            }
        }

        public void RunMenuItem(MenuItem itemNumber)
        {
            if (itemNumber != MenuItem.NotExists)
            {
                HandleEnumValue(itemNumber);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Указан некорректный пункт меню. Повторите попытку.");
                Console.ResetColor();
            }
        }

        private IDictionary<MenuItem, Action> Mapping = new Dictionary<MenuItem, Action>
        {
            { MenuItem.UserInfo, () => {  ShowUserInfo(); } },
            { MenuItem.UserAccountsInfo, () => { ShowUserAccountsInfo(); } },
            { MenuItem.UserAccountsHistory, () => { ShowUserAccountsHistory(); } },
            { MenuItem.AllHistory, () => { ShowAllHistory(); } },
            { MenuItem.UsersMoreThan, () => { ShowUsersMoreThan(); } }
        };

        private void HandleEnumValue(MenuItem enumValue)
        {
            if (_user != null)
            {
                if (Mapping.ContainsKey(enumValue))
                    Mapping[enumValue]();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Авторизация не пройдена. Продолжение невозможно!");
                Console.ResetColor();
            }
        }

        private static void ShowUserInfo()
        {
            // 1.Вывод информации о заданном аккаунте по логину и паролю;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1.Вывод информации о заданном аккаунте по логину и паролю");
            Console.ResetColor();

           // User user = _atmManager.GetUser(login: login, password: password);

            Console.WriteLine($"ИД Пользоваателя: {_user.Id}, Фамилия: {_user.SurName}, Имя: {_user.FirstName}, Отчество: {_user.MiddleName}" +
                   $", Телефон: {_user.Phone}, Паспорт: {_user.PassportSeriesAndNumber}, Дата регистрации: {_user.RegistrationDate}");
            
        }

        private static void ShowUserAccountsInfo()
        {
            // 2.Вывод данных о всех счетах заданного пользователя; 
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n2.Вывод данных о всех счетах заданного пользователя");
            Console.ResetColor();

            List<Account> accounts = _atmManager.GetAccounts(_user);

            foreach (Account account in accounts)
                Console.WriteLine($"ИД счета: {account.Id}, Дата открытия: {account.OpeningDate}, Баланс: {account.CashAll}");
            
        }

        private static void ShowUserAccountsHistory()
        {
            // 3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту; 
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту:");
            Console.ResetColor();

            var accountDetails = _atmManager.GetAccountsDetails(_user);
            foreach (var acc in accountDetails)
            {

                Console.WriteLine($"ИД счета: {acc.Key.Id}, Дата открытия: " +
                    $"{acc.Key.OpeningDate}, Баланс: {acc.Key.CashAll}");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                foreach (var history in acc.Value)
                {
                    Console.WriteLine($"\tДата операции: {history.OperationDate}, " +
                        $"Тип операции: {history.OperationType}, Сумма: {history.CashSum}");
                }
                Console.ResetColor();
            }
        }

        private static void ShowAllHistory()
        {
            Console.WriteLine("\n4.Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта:");

            var historyAndUser = from u in _atmManager.Users
                                 join a in _atmManager.Accounts on u.Id equals a.UserId
                                 join h in _atmManager.History on a.Id equals h.AccountId
                                 where h.OperationType == OperationType.InputCash
                                 orderby h.OperationDate descending
                                 select new
                                 {
                                     h.OperationDate,
                                     h.CashSum,
                                     h.OperationType,
                                     u.FirstName,
                                     u.SurName
                                 };
            foreach (var item in historyAndUser)
                Console.WriteLine($"Дата операции: {item.OperationDate}, Сумма: {item.CashSum}, Тип:{item.OperationType}, Владелец счета: {item.FirstName} {item.SurName}");

        }

        private static void ShowUsersMoreThan()
        {
            Console.WriteLine("\n5.Вывод данных о всех пользователях у которых на счёте сумма больше N:");
            Console.WriteLine("\nВведите искомую сумму и нажмите Enter:");

            if(int.TryParse(Console.ReadLine(), out int lookupSum))
            {
                var usersBalanceMoreThenN = from u in _atmManager.Users
                                            join a in _atmManager.Accounts on u.Id equals a.UserId
                                            where a.CashAll > lookupSum
                                            orderby a.CashAll
                                            select new
                                            {
                                                a.CashAll,
                                                u.FirstName,
                                                u.SurName
                                            };

                foreach (var item in usersBalanceMoreThenN)
                    Console.WriteLine($"Сумма на счёте: {item.CashAll}, Владелец счета: {item.FirstName} {item.SurName}");
            }
        }



    }
}
