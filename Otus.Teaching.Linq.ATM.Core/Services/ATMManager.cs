﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата

        // 1.Вывод информации о заданном аккаунте по логину и паролю;
        public User GetUser(string login, string password)
        {
            return Users.Where(x => x.Login == login && x.Password == password).FirstOrDefault();
        }

        // 2.Вывод данных о всех счетах заданного пользователя; 
        public List<Account> GetAccounts (User user)
        {
            return Accounts.Where(x => x.UserId == user.Id).ToList();
        }

        // 3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту; 
        public Dictionary<Account, IEnumerable<OperationsHistory>> GetAccountsDetails(User user)
        {
            return Accounts.Where(x => x.UserId == user.Id).GroupJoin(
                History,
                a => a.Id,
                h => h.AccountId,
                (account, history) => new
                {
                    account,
                    history
                }).ToDictionary(a => a.account, h => h.history);
        }

        //4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
        public Dictionary<User, IEnumerable<OperationsHistory>> GetInputOperations() 
        {
            return null;// Users.GroupJoin()
        
        
        }




        //System.Console.WriteLine("\n4.Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта:");

        //    var historyAndUser = from u in atmManager.Users
        //                         join a in atmManager.Accounts on u.Id equals a.UserId
        //                         join h in atmManager.History on a.Id equals h.AccountId
        //                         where h.OperationType == OperationType.InputCash
        //                         orderby h.OperationDate descending
        //                         select new
        //                         {
        //                             h.OperationDate,
        //                             h.CashSum,
        //                             h.OperationType,
        //                             u.FirstName,
        //                             u.SurName
        //                         };
        //    foreach (var item in historyAndUser)
        //        System.Cons





    }
}